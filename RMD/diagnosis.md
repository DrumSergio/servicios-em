---
title: "Diagnosis de Conexión a Internet"
author: "Internet Levante"
date: '2016-03-20 14:02:55'
output: pdf_document
geometry: margin=0.5in
---

Buenos días, estimado cliente.

Aquí encontrarás un informe de su conexión a Internet con nosotros:


\begin{tabular}{l|l|l|l}
\hline
Cliente & Nombre & Servicio & IP\\
\hline
14093 & RAINER CLAUS-GUSTAV GOSS & ---  10Mb/1MB & 185.104.238.104\\
\hline
\end{tabular}
    

```
## Error in is.data.frame(x): objeto 'SSH' no encontrado
```


\begin{tabular}{l|l|l|l|l|l}
\hline
IP CPE & Start & Stop & Online Time & Download & Upload\\
\hline
185.104.238.104 & 2015/09/30 11:00:16 & 2015/09/30 11:01:23 & 1.12 mins & 85.42 KB & 566.13 KB\\
\hline
185.104.238.104 & 2015/09/30 10:45:32 & 2015/09/30 10:59:42 & 14.17 mins & 541.4 KB & 1.1 MB\\
\hline
185.104.238.104 & 2015/09/27 21:40:49 & 2015/09/30 10:43:42 & 2.54 days & 4.92 GB & 378.06 MB\\
\hline
185.104.238.104 & 2015/09/27 21:17:06 & 2015/09/27 21:39:12 & 22.1 mins & 39.72 MB & 2.36 MB\\
\hline
185.104.238.104 & 2015/09/27 11:22:49 & 2015/09/27 21:15:35 & 9.88 hours & 292.63 MB & 27.65 MB\\
\hline
\end{tabular}

Gracias por confiar en nosotros

[Electrónica Martínez](http://www.electronicamartinez.com)

